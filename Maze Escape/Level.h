#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Wall.h"
#include <vector>
class Level
{

public:
	Level(sf::Vector2f vector);

	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);
	void LoadLevel(int levelNumber);

	//Setters
	void SetPosition(sf::Vector2f newPostion);

private:
	Player playerInstance;
	std::vector<Wall> wallInstances;
};

