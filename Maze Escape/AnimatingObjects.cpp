#include "AnimatingObjects.h"

AnimatingObjects::AnimatingObjects(sf::Texture& newTexture, int newFrameWidth, int newFrameHeight, float newFPS)
	: SpriteObject(newTexture)
	, frameWidth(newFrameWidth)
	, frameHeight(newFrameHeight)
	, framesPerSecond(newFPS)
	, currentFrame(0)
	, timeInFrame(sf::seconds(0.0f))
	, clips()
	, currentClip("")
	, playing(false)

{
}

void AnimatingObjects::Update(sf::Time frameTime)
{
	if (!playing)
		return;

	// If it is time for a new frame...
	timeInFrame += frameTime;
	sf::Time timePerFrame = sf::seconds(1.0f / framesPerSecond);
	if (timeInFrame >= timePerFrame)
	{
		// Update the current frame
		
		++currentFrame;
		timeInFrame = sf::seconds(0);
		// If we got to the end of the clip...
		Clip& thisClip = clips[currentClip];
		if (currentFrame > thisClip.endFrame)
		{
			// Return to the beginning of the clip
			currentFrame = thisClip.startFrame;
		}

		// Update our sprite to use the correct frame of the texture
		UpdateSpriteTextureRect();
	}
}

void AnimatingObjects::AddClip(std::string name, int startFrame, int endFrame)
{
	// Create new animation
	Clip& newClip = clips[name];
	// Setup the settings for this animation
	newClip.startFrame = startFrame;
	newClip.endFrame = endFrame;

}

void AnimatingObjects::PlayClip(std::string name)
{


	// Find the clip's information in the map
	auto pairFound = clips.find(name);
	// If the clip exists...
	if (pairFound != clips.end()&&currentClip!=name)
	{
		// Set up the animation based on the clip
		currentClip = name;
		currentFrame = pairFound->second.startFrame;
		timeInFrame = sf::seconds(0.0f);
		playing = true;
		// Update sprite rect
		UpdateSpriteTextureRect();
	}
	if (pairFound != clips.end() && currentClip == name)
	{
		Resume();
	}
}

void AnimatingObjects::Pause()
{
	playing = false;
}

void AnimatingObjects::Stop()
{
	playing = false;
	// Reset to first frame of the animation
	Clip& thisClip = clips[currentClip];
	currentFrame = thisClip.startFrame;
	UpdateSpriteTextureRect();

}

void AnimatingObjects::Resume()
{
	if (!currentClip.empty())
		playing = true;

}

void AnimatingObjects::UpdateSpriteTextureRect()
{
	int numFramesX = sprite.getTexture()->getSize().x / frameWidth;
	int xFrameIndex = currentFrame % numFramesX;
	int yFrameIndex = currentFrame / numFramesX;
	sf::IntRect textureRect;
	textureRect.left = xFrameIndex * frameWidth;
	textureRect.top = yFrameIndex * frameHeight;
	textureRect.width = frameWidth;
	textureRect.height = frameHeight;
	sprite.setTextureRect(textureRect);
}



