#pragma once
#include <SFML/Graphics.hpp>
#include "AnimatingObjects.h"

class Player : public AnimatingObjects
{
public:
	// Constructors / Destructors
	Player(sf::Vector2f startingPos);
	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void HandleSolidCollision(sf::FloatRect otherHitbox);
	//setters
	void SetPosition(sf::Vector2f newPostion);
private:
	// Data
	sf::Vector2f velocity;
	float speed;
	sf::Vector2f previousPosition;
};

