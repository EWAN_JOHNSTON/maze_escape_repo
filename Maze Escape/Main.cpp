#include <SFML/Graphics.hpp>
#include "AssetManager.h"
#include "SpriteObject.h"
#include "Player.h"
#include "AnimatingObjects.h"
#include "Wall.h"
#include "Level.h"

int main()
{
    //Game Setup
    sf::RenderWindow gameWindow;
    sf::Clock gameClock;
    gameWindow.create(sf::VideoMode::getDesktopMode(), "Maze Escape", sf::Style::Titlebar | sf::Style::Close);

    //Level Setup
    Level testLevel(sf::Vector2f(gameWindow.getSize().x / 2, gameWindow.getSize().y / 2));
    testLevel.LoadLevel(2);

    
    while (gameWindow.isOpen())
    {
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                gameWindow.close();
        }
//----------------------INPUT SECTION--------------------------------------------------------------------------
        testLevel.Input();
//----------------------UPDATE SECTION-------------------------------------------------------------------------
        sf::Time frameTime=gameClock.restart();
        testLevel.Update(frameTime);
//----------------------DRAW SECTION---------------------------------------------------------------------------
        gameWindow.clear();
        testLevel.DrawTo(gameWindow);
        gameWindow.display();

    }
    return 0;
}