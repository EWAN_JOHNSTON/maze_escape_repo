#include "Wall.h"
#include "AssetManager.h"

Wall::Wall(sf::Vector2f startingPos)
	: SpriteObject(AssetManager::RequestTexture("MEA/Graphics/Wall.png"))
{
	sprite.setPosition(startingPos);
}
